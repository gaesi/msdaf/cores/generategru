from flask import Flask,render_template,Response,request
import requests

app = Flask(__name__)

@app.route("/")
def hello():

    return render_template('form.html')


@app.route("/request", methods=['POST'])
def request():


    data={
        'codigo_favorecido': request.form['codigo_favorecido'],
        'gestao': request.form['gestao'],
        'codigo_correlacao': request.form['codigo_correlacao'],
        'nome_favorecido': request.form['nome_favorecido'],
        'codigo_recolhimento': request.form['codigo_recolhimento'],
        'nome_recolhimento': request.form['nome_recolhimento'],
        'referencia': request.form['referencia'],
        'competencia': request.form['competencia'],
        'vencimento': request.form['vencimento'],
        'cnpj_cpf': request.form['cnpj_cpf'],
        'nome_contribuinte': request.form['nome_contribuinte'],
        'valorPrincipal': request.form['valorPrincipal'],
        'descontos': request.form['descontos'],
        'deducoes': request.form['deducoes'],
        'multa': request.form['multa'],
        'juros': request.form['juros'],
        'acrescimos': request.form['acrescimos'],
        'valorTotal': request.form['valorTotal'],
        'boleto': '1',
        'impressao': 'SA',
        'pagamento': '0',
        'campo': 'NRCR',
        'ind': '0'
    }


    # data={
    # 'codigo_favorecido': '257001',
    # 'gestao': '00001',
    # 'codigo_correlacao': '4399',
    # 'nome_favorecido': 'DIRETORIA EXECUTIVA DO FUNDO NAC. DE SAUDE',
    # 'codigo_recolhimento': '28852-7',
    # 'nome_recolhimento': '(unable to decode value)',
    # 'referencia': '0001',
    # 'competencia': '07/2019',
    # 'vencimento': '31/07/2019',
    # 'cnpj_cpf': '236.819.870-92',
    # 'nome_contribuinte': 'Teste teste',
    # 'valorPrincipal': '0,01',
    # 'descontos': '',
    # 'deducoes': '',
    # 'multa': '',
    # 'juros': '',
    # 'acrescimos': '',
    # 'valorTotal': '0,01',
    # 'boleto': '3',
    # 'impressao': 'SA',
    # 'pagamento': '0',
    # 'campo': 'NRCR',
    # 'ind': '0'
    # }

    root = requests.post('http://consulta.tesouro.fazenda.gov.br/gru_novosite/gerarPDF.asp', data = data)

    def pdf_response(pdf, filename):
    	resp = Response(pdf)
    	resp.headers['Content-Disposition'] = "inline; filename=%s" % filename
    	resp.mimetype = 'application/pdf'
    	return resp
    return pdf_response(root,"teste")
    # http://consulta.tesouro.fazenda.gov.br/gru_novosite/gerarHTML.asp
